This repo contains Space Shooter 2D Tutorial project files.

###########################################################

Links to downloadable assets:

- https://opengameart.org/content/space-shooter-redux
- https://opengameart.org/content/four-level-meter-indicator
- https://opengameart.org/content/alien-spaceship-sprite-pack
- https://opengameart.org/content/2d-space-shooter-assets
- https://opengameart.org/content/boxy-bold-font
- https://opengameart.org/content/rpg-portraits
- https://opengameart.org/content/ui-pack-space-extension
- https://opengameart.org/content/4-flat-design-planets
- https://opengameart.org/content/teamred-scout
