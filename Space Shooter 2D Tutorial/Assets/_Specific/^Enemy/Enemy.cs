﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Enemy : MonoBehaviour
{
	public float MovementSpeed = 4f;

	private void Update()
	{
		this.transform.Translate(this.transform.up * this.MovementSpeed * Time.deltaTime);
	}
}