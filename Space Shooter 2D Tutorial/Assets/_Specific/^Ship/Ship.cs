﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ship : MonoBehaviour
{
	public int Health;
	public int MaxHealth;

	public Character Character;

	public void Die()
	{
		Destroy(this.gameObject);
	}

	public void TakeDamage(int amount)
	{
		if (amount <= 0)
			return;

		this.Health -= amount;

		if (this.Health < 0)
		{
			this.Health = 0;

			this.Die();
		}

		if (this.Character != null)
		{
			this.Character.UpdateHealthUI(this.Health);
		}
	}

	public void Heal(int amount)
	{
		if (amount <= 0)
			return;

		this.Health += amount;

		if (this.Health >= this.MaxHealth)
		{
			this.Health = this.MaxHealth;
		}

		if (this.Character != null)
		{
			this.Character.UpdateHealthUI(this.Health);
		}
	}

	private void Awake()
	{
		if (this.Character != null)
			this.Character.UpdateHealthUI(this.Health);
	}
}