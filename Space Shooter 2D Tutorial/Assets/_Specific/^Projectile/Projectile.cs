﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Projectile : MonoBehaviour
{
	public float Speed = 4f;

	private void Update()
	{
		this.transform.Translate(this.transform.up * this.Speed * Time.deltaTime);
	}

	private void Awake()
	{
		Object.Destroy(this.gameObject, 4f);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Ship ship = collision.GetComponent<Ship>();
		if (ship != null)
		{
			ship.TakeDamage(1);
		}

		Destroy(this.gameObject);
	}
}