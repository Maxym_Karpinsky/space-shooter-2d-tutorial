﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
	public Image HealthBarImage;

	public Sprite[] HealthBarSprites;

	public float MaxXPosition;
	public float MaxYPosition;

	public GameObject ProjectilePrefab;

	public Transform[] ShootingPoints;

	public void Shoot()
	{
		for (int i = 0; i < this.ShootingPoints.Length; i++)
		{
			GameObject.Instantiate(this.ProjectilePrefab, this.ShootingPoints[i].position, Quaternion.identity);
		}
	}

	public void UpdateHealthUI(int health)
	{
		if (this.HealthBarImage != null && health > 0)
			this.HealthBarImage.sprite = this.HealthBarSprites[health - 1];
	}

	public float MovementSpeed = 5f;

	private void Update()
	{
		// Movement

		Vector3 targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		targetPosition = new Vector3(Mathf.Clamp(targetPosition.x, -this.MaxXPosition, this.MaxXPosition), Mathf.Clamp(targetPosition.y, -this.MaxYPosition, this.MaxYPosition), 0f);

		this.transform.position = Vector3.Lerp(this.transform.position, targetPosition, this.MovementSpeed * Time.deltaTime);

		// Shooting

		if (Input.GetMouseButtonDown(0))
		{
			this.Shoot();
		}
	}

	private void Awake()
	{
		Cursor.visible = false;
	}
}